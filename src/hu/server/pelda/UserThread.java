package hu.server.pelda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.List;

//TODO: Szétbontás
public class UserThread extends Thread {

	private Socket socket;
	private String nickName;
	private List<UserThread> userThreads;
	private final static String ARRIVED= "Belépett a chat-re!";
	private List<Message> history;
	
	public UserThread(Socket socket, String nickName, List<UserThread> userThreads, List<Message> history) {
		super();
		this.socket = socket;
		this.nickName = nickName;
		this.userThreads = userThreads;
		this.history = history;
	}
	
	@Override
	public void run() {
		
		//TODO: Megoldani a kiemelését, megtervezni hova, hogyan, pl: MessageSender -be kiszervezni
		try {
			Message message = new Message(new Date(), this.nickName, ARRIVED);
			this.history.add(message);
			sendBroadcast(message.toString());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		//TODO: Megoldani a kiemelését, megtervezni hova, hogyan, pl: MessageSender -be kiszervezni
		try {
			PrintWriter pw = new PrintWriter(this.socket.getOutputStream(), true);
			for (Message message : this.history) {
				pw.println(message.toString());
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		while (!this.socket.isClosed()) {
			//mivel a socket megvalósítja az AutoClosable interface-t, ezért használhatjuk a try resource részében
			try (BufferedReader br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()))) {
				String sor;
				while ((sor = br.readLine()) != null) {
					Message message = new Message(new Date(), this.nickName, sor);
					this.history.add(message);
					
					sendBroadcast(message.toString());
				}
			} catch (IOException e) {
				this.userThreads.remove(this);
				e.printStackTrace();
			}
		}
		this.userThreads.remove(this);
	}
	
	private void sendBroadcast(String msg) throws IOException {
		System.out.println(this.userThreads);
		for (UserThread user : this.userThreads) {
			
			if (!user.getSocket().equals(this.socket)) { /*vagy: !user.equals(this)*/
				PrintWriter pw = new PrintWriter(user.getSocket().getOutputStream(), true);
				pw.println(msg);
			}

		}
	}

	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
}
