package hu.server.pelda;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//TODO: NickNameRegister átgondolása, jobb megvalósáítás, áthelyezés, szétszedés
public class ChatServer {

	private ServerSocket serverSocket;
	private int port;
	private List<UserThread> users;
	private List<Message> history;

	public ChatServer(int port) throws IOException {
		this.port = port;
		this.serverSocket = new ServerSocket(port);
		this.users = Collections.synchronizedList(new ArrayList<UserThread>());
		this.history = Collections.synchronizedList(new ArrayList<Message>());
	}

	public void start() {
		System.out.println("Server elindult a "+this.port+" -on");
		while(true) {
			try {
				this.waitingUsers();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void waitingUsers() throws IOException {
		System.out.println("Várakozás a felhasználóra...");
		Socket socket = this.serverSocket.accept();
		NickNameRegister nickNameRegister = new NickNameRegister(socket, this.users);
		if(nickNameRegister.tryRegister()) {
			UserThread u = new UserThread(socket, nickNameRegister.getNickName(), this.users, this.history);
			this.users.add(u);
			u.start();
		} 
	}

}
