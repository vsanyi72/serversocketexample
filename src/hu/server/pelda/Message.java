package hu.server.pelda;

import java.util.Date;

public class Message {
	
	private int id;
	private Date timestamp;
	private String nickName;
	private String message;
	public Message(int id, Date timestamp, String nickName, String message) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.nickName = nickName;
		this.message = message;
	}
	public Message(Date timestamp, String nickName, String message) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.nickName = nickName;
		this.message = message;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return new StringBuilder()
		.append("[")
		.append(this.timestamp)
		.append("]")
		.append(" ")
		.append(this.nickName)
		.append(" - ")
		.append(this.message)
		.toString();
	}

}
