package hu.server.pelda;

import java.io.IOException;

//TODO: Az egyes felhasználó be illetve kilépéseket küldjük ki az összes felhasználónak
//TODO: A beszélgetések tárolása és az újonan becsatlakozott felhaszálók rendelkezésre bocsátása
//TODO: Becenév küldés átalakítása - KÉSZ
//TODO: Házi feladat átgondolni, hogyan lehetne megcsinálni, hogy a server is tudjon chatelni
public class Start {

	public static void main(String[] args) {
		
		try {
			ChatServer server = new ChatServer(9999);
			server.start();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
