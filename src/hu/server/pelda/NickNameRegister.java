
package hu.server.pelda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class NickNameRegister {
	
	private final static String STATUS_SUCCESS= "Sikeresen csatlakoztál!";
	private final static String STATUS_FAILED= "Ilyen nickname már szerepel!";
	
	private Socket socket;
	private String nickName;
	private List<UserThread> users;
	
	public NickNameRegister(Socket socket, List<UserThread> users) {
		this.socket = socket;
		this.users = users;
		this.nickName = readNickname();
		
	}
	
	/**
	 * Megpróbálja beregisztrálni a nicknevet, ha sikerül akkor true, ha nem akkor false
	 * 
	 * @return
	 * @throws IOException 
	 */
	public boolean tryRegister() throws IOException {
		if(checkNickname()) {
			writeNicknameVerification(STATUS_SUCCESS);
			return true;
		} else {
			writeNicknameVerification(STATUS_FAILED);
			this.socket.close();
			return false;
		}
	}
	
	private boolean checkNickname() {
		if(this.nickName.isEmpty()) {
			return false;
		}
		
		for(UserThread u : this.users) {
			if(u.getNickName().equals(this.nickName)) {
				return false;
			}
		}
		return true;
	}
	
	private String readNickname() {
		String nickname="";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			
			nickname = br.readLine();
			System.out.println("Nickname: "+nickname);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return nickname;
	}
	
	private void writeNicknameVerification(String status) {
		try {
			PrintWriter pw = new PrintWriter(this.socket.getOutputStream(), true);
			pw.println(status);
			pw.flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public List<UserThread> getUsers() {
		return users;
	}

	public void setUsers(List<UserThread> users) {
		this.users = users;
	}
	
}
